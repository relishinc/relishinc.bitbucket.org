var APP = ( function () {
	
	var
		screens;
  
  var _init = function ( $options ) 
  {
    screens = $('.device > .screen');
    
    _initEvents();
    _gotoScreen($options.start || screens.first().data('name'));
    
  };
  
  var _initEvents = function()
  {
	  screens
	  	.on('animateIn', function() {

		  	var
		  		screen		= $(this),
		  		animClass = screen.data('animatein') || 'fadeIn';		
		  		
		  	screen
		  		.addClass('screen--visible animate-in');
		  		
		  	_animateScreen(screen, animClass, function() { });
		  	
	  	})
	  	.on('animateOut', function() {
		  	
		  	var
		  		screen		= $(this),
		  		animClass = screen.data('animateout') || 'fadeOut';

		  	screen
		  		.addClass('animate-out');
		  				  	
		  	_animateScreen(screen, animClass, function() { screen.removeClass('screen--visible'); });
		  	
	  	});
	  	
	  $(document)
	  	.on('click', '[data-gotoScreen]', function(e) {
		  	e.preventDefault();
		  	
		  	_gotoScreen($(this).data('gotoscreen'));
		  	
	  	});
	  
  };
  
  var _animateScreen = function( $screen, $animClass, $callback )
  {
	  $screen
	  	.addClass( $animClass + ' animated' )
	  	.one('webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend', function() {
		  	
		  	$screen.removeClass( $animClass + ' animated animate-in animate-out' );
		  	
		  	if ( $callback != undefined ) 
		  	{
			  	$callback();
			  }
			  
	  	});
  };
  
  var _gotoScreen = function( $name ) 
  {
	  screens
	  	.filter('.screen--visible')
	  	.trigger('animateOut');
	  
    screens
    	.filter('[data-name="' + $name + '"]')
    	.trigger('animateIn');
  };

  return {
    init: _init,
    gotoScreen: _gotoScreen
  }

} )();
