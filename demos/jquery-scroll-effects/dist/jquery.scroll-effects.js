/* Name: jquery.scroll-effects */
/* Version: 0.0.1 */
/* Generated Wed, 20 Mar 2019 22:01:04 GMT */
"use strict";

var SCROLL = function () {
  var _init = function _init($options) {
    SCROLL.options = $options || {};
    $('[data-scroll], [data-scroll-from], [data-scroll-to]').not('.scroll_anim-ready').addClass('scroll_anim-ready').each(function (index, el) {
      //<div 
      //	data-scroll-from='{ "y": 100, "opacity": 0 }'   // the properties to animate (starting values)
      // 	data-scroll-start="0"												    // when to start animation [ 0 = when element STARTS to enter viewport ]
      //  data-scroll-end="1"												      // when to stop animation [ 1 = when element STARTS to leave viewport ]
      // 	data-scroll-exit="true"													// calculate "end" based on when element completely LEAVES viewport
      //>
      try {
        var tl = new TimelineLite(),
            progress;

        if ($(el).data('scroll-to')) {
          tl.to($(el), 1, $(el).data('scroll-to'));
        } else {
          tl.from($(el), 1, $(el).data('scroll') || $(el).data('scroll-from'));
        }

        tl.pause(); //$(el).css({ transform: 'translate3d(0,0,0)', backfaceVisibility: 'hidden' })

        $(window).on('scroll resize', function (e) {
          var wh = $(window).height(),
              st = $(window).scrollTop(),
              et = $(el).offset().top,
              eh = $(el).outerHeight(),
              end = $.isNumeric($(el).data('scroll-end')) ? $(el).data('scroll-end') : 1,
              start = $.isNumeric($(el).data('scroll-start')) ? $(el).data('scroll-start') : 0,
              enter = et + wh * (start - 1),
              // when element enters "start" point
          exit = $(el).data('scroll-exit') ? // when element leaves "end" point 
          et + eh + wh * (end - 1) : // element completely leaves viewport
          et + wh * (end - 1); // element starts to leave viewport

          progress = Math.max(0, Math.min(1, (st - enter) / (exit - enter)));
          tl.progress(progress);
        });
      } catch (e) {
        console.log('Could not animate on scroll:', e);
      }
    });
  };

  return {
    init: _init
  };
}();