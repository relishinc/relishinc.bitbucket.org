# Mobile UI Prototype

A simple, single web page "app" that allows you quickly build a mobile app prototype. You can transition from screen to screen and set up hotspots that link to other screens. Screens can be a single image (like a fullscreen mockup) or built as HTML.

An example can be found here: http://clients.reli.sh/nerve

### Set up

If you plan to tweak the CSS or JS, you'll need to run the following:

```
cd src
bower install
npm install
gulp watch
```

### How it works

Most of the functionality is baked into the HTML.

**Screens**

Identify each "screen" in the demo app with the `data-name` attribute: 
```
<div data-name="home">I'm the home screen</div>
```

**Linking to another screen**

Use the `data-gotoScreen` attribute on any tag to link to another screen: 
```
<div data-gotoScreen="home">Send me home</div>
```

**Screen transitions**

Use the `data-animateIn` and `data-animateOut` attribute to specify the animate.css animation: 
```
<div data-screen="home" data-animateIn="fadeIn" data-animateOut="zoomOut" />
```
Of course you can roll your own custom CSS3 animations too.

**Note:** While a screen is animating in and out of view it gets an `animate-in` and `animate-out` CSS class attached to it so that you can have elements within the screen animate in and out of view as well.

