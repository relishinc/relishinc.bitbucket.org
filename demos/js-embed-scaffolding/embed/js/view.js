/*
 *  Embed viewer
 *
 */ 
 
(function(global) {
	
	var
		Embed,
		settings;

  // Globals
  
  if( ! global.Embed ) 
  { 
	  global.Embed = {}; 
	};
	
  Embed 		= global.Embed;
  settings 	= Embed.settings;
  
  // Render the embed on the page
  
  function renderEmbed( embedData )
  {
	  var
	  	_iframe = document.createElement('iframe'),
	  	_width	= '100%', // default width (100% of containing element)
	  	_height = 200, 		// default height (in px)
	  	styles = {				// CSS styles
			  minWidth:		'300px',
			  maxWidth:		'600px',
			  minHeight:	'200px'
	  	}
	  	
		// Example: use the "type" query param to figure out which file to embed
		
		if( embedData.params.type )
		{
			embedUrl = settings.baseUrl + 'widgets/' + embedData.params.type + '.html';
		}
		else
		{
			embedUrl = settings.baseUrl + 'widgets/default.html';
		}
		
		// Get container width: 
		// embedData.container.offsetWidth

	 	// Set the iFrame attributes

	  _iframe.src 					= embedUrl;
	  _iframe.width 				= _width;
	  _iframe.height 				= _height;
	  _iframe.frameBorder 	= 0;
	  _iframe.style.border = 'none';
	  
	  for (var i in styles)
	  {
	  	_iframe.style[i] = styles[i];
	  }
	  
	  // Add it to the DOM
	  
	  embedData.container.appendChild(_iframe);	  
  }
  
	if( ! Embed.rendered )
	{
		var
			embedData,
			embedUrl,
			numEmbeds = settings.length;
		
		for ( var i = 0; i < numEmbeds; i++ )
		{
			// settings[i] contains the query string params from our original embed and a 
			// reference to the containing element that will hold the iframe

			renderEmbed(settings[i]);
		}
		
	}
	
	Embed.rendered = true;

}(this)); 
 