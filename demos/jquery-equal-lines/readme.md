# Equal Lines Plugin

Ensure multi-line headings are displayed with each line having the same length. It's kind of like getting rid of [widows](https://en.wikipedia.org/wiki/Widows_and_orphans) but taking it a few steps too far.

Why? Why not.

### Installation

Install via Bower

```bash
bower install https://bitbucket.org/relishinc/jquery-equal-lines.git --save
```

### Usage

jQuery plugin can be used like so:

```javascript
$('.some-selector')
	.equalLines(options)
```

### Options

```javascript
{
    precision: 		2,  // the number of pixels to "step" by when calculating the ideal layout
	throttling: 	300 // how often (in ms) the text should be reformatted while the window resizes
}
```