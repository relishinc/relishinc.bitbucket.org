/*!
 * jQuery Equal Lines - v0.9.1 - 2016-08-31
 * https://bitbucket.org/relishinc/jquery-equal-lines
 * Copyright (c) 2016 Steve Palmer
 * Licensed MIT
 */

(function ( $ ) {
 
  $.fn.equalLines = function( options ) {
	  
    var 
    	elements = this,
	    settings = $.extend({
		    precision: 		2,
		    debug: 				false,
		    throttling: 	300
	    }, options )
	    ;
        
		elements
			.each(function() {
				
				if ( ! $(this).data('equalLines.initialized') )
				{
					$(this)
						.data('equalLines.initialized', true)
						.wrapInner('<span data-equal-lines-wrapper style="display:inline-block; max-width: 100%;" />');						
				}
				
				if ( settings.debug )
				{
					$(this)
						.css('background', 'rgba(255,0,0,0.25)'); // for testing
				}
				
			})
    	.on('equalLines.updateLayout', function() {
	    	
        var
        	self			= $(this),
					wrapper   = self.find('[data-equal-lines-wrapper]'),
	    		oldHeight = 0;
	    		
	    	// reset the wrapper
	    	
	    	wrapper
	    	  .css('width', '100%');

				if ( settings.debug )
				{
					$(this)
						.css('background', 'rgba(255,0,0,0.5)'); // for testing
				}	    	  

	    	// get the element's height
	    		
	    	oldHeight = self.height();
	    	
	    	// decrease the width until it breaks onto a new line
	    	
	    	while ( $(this).height() === oldHeight )	
	    	{
  	    	wrapper
  	    	  .width(wrapper.width() - settings.precision);
	    	}
	    	
	    	// now increase the width one increment
	    	
	    	wrapper
	    	  .width(wrapper.width() + settings.precision);

    	})
    	.trigger('equalLines.updateLayout');

		function throttle ($callback, $limit) 
		{
		  var 
		  	wait = false;    
		  
		  return function () 
		  {
		    if ( ! wait) 
		    { 
		      $callback.call();
		      wait = true;         
		      setTimeout(function () {
		      	wait = false; 
		      }, $limit);
		    
		    }
		    
		  };
		  
		}

    $(window)
	    .on('resize', throttle(function() {
		    
				elements
					.trigger('equalLines.updateLayout');		
					    
	    }, settings.throttling));    	 
	    
    return this;
  };
 
}( jQuery ));

